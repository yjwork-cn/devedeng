# Portuguese/Brazil translation of devede.
# Copyright (C) 2006 THE DeVeDe's COPYRIGHT HOLDER
# This file is distributed under the same license as the devede package.
# Marco de Freitas <marcodefreitas@gmail.com>, 2008.
# , fuzzy
#
#
msgid ""
msgstr ""
"Project-Id-Version: DeVeDe 3.12\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-13 22:07+0200\n"
"PO-Revision-Date: 2009-07-07 13:27-0300\n"
"Last-Translator: Marco de Freitas <marcodefreitas@gmail.com>\n"
"Language-Team: Portuguese/Brazil <gnome-l10n-br@listas.cipsga.org.br>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Portuguese\n"
"X-Poedit-Country: BRAZIL\n"
"X-Poedit-SourceCharset: utf-8\n"

#: ../src/devedeng/add_files.py:44
msgid "Video files"
msgstr "Arquivos de vídeo"

#: ../src/devedeng/add_files.py:49 ../src/devedeng/ask_subtitles.py:92
#: ../src/devedeng/dvd_menu.py:146 ../src/devedeng/dvd_menu.py:158
#: ../src/devedeng/opensave.py:48
msgid "All files"
msgstr "Todos os arquivos"

#: ../src/devedeng/ask_subtitles.py:81
#, fuzzy
msgid "Subtitle files"
msgstr "Legenda"

#: ../src/devedeng/avconv.py:114 ../src/devedeng/ffmpeg.py:114
#, python-format
msgid "Converting %(X)s (pass 2)"
msgstr ""

#: ../src/devedeng/avconv.py:117 ../src/devedeng/ffmpeg.py:117
#, python-format
msgid "Converting %(X)s (pass 1)"
msgstr ""

#: ../src/devedeng/avconv.py:127
#, python-format
msgid "Converting %(X)s"
msgstr ""

#: ../src/devedeng/avconv.py:507
#, fuzzy, python-format
msgid "Creating menu %(X)d"
msgstr "Criando menu %(menu_number)d"

#: ../src/devedeng/choose_disc_type.py:156
#: ../src/devedeng/choose_disc_type.py:167
#: ../src/devedeng/choose_disc_type.py:183
#: ../src/devedeng/choose_disc_type.py:194
#: ../src/devedeng/choose_disc_type.py:205
#: ../src/devedeng/choose_disc_type.py:215
#: ../src/devedeng/choose_disc_type.py:221
#: ../src/devedeng/choose_disc_type.py:227
#, python-format
msgid "\t%(program_name)s (not installed)\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:159
#: ../src/devedeng/choose_disc_type.py:170
#: ../src/devedeng/choose_disc_type.py:186
#: ../src/devedeng/choose_disc_type.py:197
#: ../src/devedeng/choose_disc_type.py:208
#: ../src/devedeng/choose_disc_type.py:218
#: ../src/devedeng/choose_disc_type.py:224
#: ../src/devedeng/choose_disc_type.py:230
#, python-format
msgid "\t%(program_name)s (installed)\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:161
#, python-format
msgid ""
"Movie identifiers (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:172
#, python-format
msgid ""
"Movie players (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:188
#, python-format
msgid ""
"Movie Converters (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:200
#, python-format
msgid ""
"CD/DVD burners (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:210
#, python-format
msgid ""
"ISO creators (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:233
#, python-format
msgid ""
"Other programs:\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/dvdauthor_converter.py:48
#, fuzzy
msgid "Creating DVD structure"
msgstr "Criando árvore de diretórios de DVD"

#: ../src/devedeng/dvd_menu.py:51 ../src/devedeng/dvd_menu.py:418
msgid "Play all"
msgstr ""

#: ../src/devedeng/dvd_menu.py:93
msgid "The selected file is a video, not an audio file"
msgstr ""

#: ../src/devedeng/dvd_menu.py:93 ../src/devedeng/dvd_menu.py:96
msgid "Error"
msgstr "Erro"

#: ../src/devedeng/dvd_menu.py:96
#, fuzzy
msgid "The selected file is not an audio file"
msgstr "O arquivo parece ser de áudio."

#: ../src/devedeng/dvd_menu.py:141
#, fuzzy
msgid "Picture files"
msgstr "Arquivos de vídeo"

#: ../src/devedeng/dvd_menu.py:153
#, fuzzy
msgid "Sound files"
msgstr "Arquivos de vídeo"

#: ../src/devedeng/dvd_menu.py:409
#, python-brace-format
msgid "Page {0} of {1}"
msgstr ""

#: ../src/devedeng/dvd_menu.py:416
msgid "Play_all"
msgstr ""

#: ../src/devedeng/file_copy.py:31
#, fuzzy, python-format
msgid "Copying file %(X)s"
msgstr "Copiando o arquivo"

#: ../src/devedeng/genisoimage.py:73 ../src/devedeng/mkisofs.py:73
#, fuzzy
msgid "Creating ISO image"
msgstr "Criando arquivo ISO"

#: ../src/devedeng/help.py:37
#, fuzzy
msgid "Can't open the help files."
msgstr "Não é possível abrir o arquivo."

#: ../src/devedeng/mux_dvd_menu.py:34
#, python-format
msgid "Mixing menu %(X)d"
msgstr ""

#: ../src/devedeng/opensave.py:44
msgid "DevedeNG projects"
msgstr ""

#: ../src/devedeng/project.py:231
msgid "Abort the current DVD and exit?"
msgstr "Cancelar o DVD atual e sair?"

#: ../src/devedeng/project.py:231
msgid "Exit DeVeDe"
msgstr "Sair do DeVeDe"

#: ../src/devedeng/project.py:285 ../src/devedeng/project.py:738
msgid "In menu"
msgstr ""

#: ../src/devedeng/project.py:287
msgid "The following files could not be added:"
msgstr ""

#: ../src/devedeng/project.py:288 ../src/devedeng/project.py:746
msgid "Error while adding files"
msgstr ""

#: ../src/devedeng/project.py:299
#, python-format
msgid "The file <b>%(X)s</b> <i>(%(Y)s)</i> will be removed."
msgstr ""

#: ../src/devedeng/project.py:299
#, fuzzy
msgid "Delete file"
msgstr "Excluir título"

#: ../src/devedeng/project.py:499
#, python-format
msgid "The limit for this format is %(l)d files, but your project has %(h)d."
msgstr ""

#: ../src/devedeng/project.py:499
msgid "Too many files in the project"
msgstr ""

#: ../src/devedeng/project.py:508
#, python-format
msgid ""
"The selected folder already exists. To create the project, Devede must "
"delete it.\n"
"If you continue, the folder\n"
"\n"
" <b>%s</b>\n"
"\n"
" and all its contents <b>will be deleted</b>. Continue?"
msgstr ""

#: ../src/devedeng/project.py:508
#, fuzzy
msgid "Delete folder"
msgstr "Excluir título"

#: ../src/devedeng/project.py:643
msgid "Close current project and start a fresh one?"
msgstr ""

#: ../src/devedeng/project.py:643
msgid "New project"
msgstr ""

#: ../src/devedeng/project.py:678
msgid "The file already exists. Overwrite it?"
msgstr ""

#: ../src/devedeng/project.py:678
msgid "The file already exists"
msgstr ""

#: ../src/devedeng/project.py:744 ../src/devedeng/project.py:766
msgid "Page jump"
msgstr ""

#: ../src/devedeng/project.py:746
msgid "The following files in the project could not be added again:"
msgstr ""

#: ../src/devedeng/runner.py:90 ../src/devedeng/runner.py:91
#, fuzzy
msgid "Cancel the current job?"
msgstr "Cancelar a tarefa?"

#: ../src/devedeng/settings.py:45 ../src/devedeng/settings.py:63
msgid "Use all cores"
msgstr ""

#: ../src/devedeng/settings.py:50
#, python-format
msgid "Use %(X)d core"
msgid_plural "Use %(X)d cores"
msgstr[0] ""
msgstr[1] ""

#: ../src/devedeng/settings.py:57
#, python-format
msgid "Use all except %(X)d core"
msgid_plural "Use all except %(X)d cores"
msgstr[0] ""
msgstr[1] ""

#: ../src/devedeng/settings.py:128
msgid "No discs supported"
msgstr ""

#: ../src/devedeng/subtitles_mux.py:43
#, fuzzy, python-format
msgid "Adding %(L)s subtitles to %(X)s"
msgstr "Adicionando legendas a"

#: ../src/devedeng/title.py:32
#, python-format
msgid "Title %(X)d"
msgstr "Título %(X)d"

#: ../src/devedeng/vcdimager_converter.py:48
#, fuzzy
msgid "Creating CD image"
msgstr "Criando arquivo ISO"

#~ msgid "Creating BIN/CUE files"
#~ msgstr "Criando arquivos BIN/CUE"

#~ msgid ""
#~ "Failed to create the BIN/CUE files\n"
#~ "Maybe you ran out of disk space"
#~ msgstr ""
#~ "Falha ao criar os arquivos BIN/CUE\n"
#~ "Talvez o espaço livre no disco não seja suficiente"

#~ msgid ""
#~ "Failed to create the ISO image\n"
#~ "Maybe you ran out of disk space"
#~ msgstr ""
#~ "Falha ao criar a imagem ISO\n"
#~ "Talvez o espaço livre no disco não seja suficiente"

#~ msgid ""
#~ "Insuficient free space. To create this disc\n"
#~ "%(total)d MBytes are needed, but only %(free)d MBytes are available."
#~ msgstr ""
#~ "Espaço livre insuficiente. Para criar este disco\n"
#~ "%(total)d MBytes são necessários, mas apenas %(free)d MBytes estão "
#~ "disponíveis."

#~ msgid ""
#~ "Failed to write to the destination directory.\n"
#~ "Check that you have privileges and free space there."
#~ msgstr ""
#~ "Falha ao gravar no diretório de destino.\n"
#~ "Verifique se há permissões e espaço livre suficiente."

#~ msgid ""
#~ "The file or folder\n"
#~ "\n"
#~ "%(folder)s\n"
#~ "\n"
#~ "already exists. If you continue, it will be deleted."
#~ msgstr ""
#~ "O arquivo ou diretório\n"
#~ "\n"
#~ "%(folder)s\n"
#~ "\n"
#~ "já existe. Este será apagado caso continue."

#~ msgid "Unknown error"
#~ msgstr "Erro desconhecido"

#~ msgid ""
#~ "Failed to create the DVD tree\n"
#~ "Maybe you ran out of disk space"
#~ msgstr ""
#~ "Falha ao criar a árvore de diretórios do DVD\n"
#~ "Talvez o espaço livre no disco não seja suficiente"

#~ msgid ""
#~ "The menu soundtrack seems damaged. Using the default silent soundtrack."
#~ msgstr "A trilha sonora do menu parece estar corrompida. "

#~ msgid ""
#~ "Can't find the menu background.\n"
#~ "Check the menu options."
#~ msgstr ""
#~ "Não é possível encontrar o plano de fundo do menu.\n"
#~ "Verifique as opções do menu."

#~ msgid "That file doesn't contain a disc structure."
#~ msgstr "Este arquivo não contém uma estrutura de disco."

#~ msgid "That file doesn't contain a DeVeDe structure."
#~ msgstr "Este arquivo não contém uma estrutura do DeVeDe."

#~ msgid ""
#~ "Can't find the following movie files. Please, add them and try to load "
#~ "the disc structure again.\n"
#~ msgstr ""
#~ "Não foi possível encontrar os seguintes arquivos de vídeo. Por favor, "
#~ "adicione-os e tente carregar a estrutura de disco novamente.\n"

#~ msgid ""
#~ "Can't find the menu background. I'll open the disc structure anyway with "
#~ "the default menu background, so don't forget to fix it before creating "
#~ "the disc."
#~ msgstr ""
#~ "Não foi possível encontrar o plano de fundo do menu. A estrutura de disco "
#~ "será aberta mesmo assim, usando o plano de fundo padrão. Lembre-se de "
#~ "corrigir o plano de fundo do menu antes de criar o disco."

#~ msgid ""
#~ "Can't find the menu soundtrack file. I'll open the disc structure anyway "
#~ "with a silent soundtrack, so don't forget to fix it before creating the "
#~ "disc."
#~ msgstr ""
#~ "Não foi possível encontrar a trilha sonora do menu. A estrutura de disco "
#~ "será aberta mesmo assim, usando silêncio como trilha sonora. Lembre-se de "
#~ "corrigir a trilha sonora do menu antes de criar o disco."

#~ msgid "No filename"
#~ msgstr "Sem nome de arquivo"

#~ msgid "Can't save the file."
#~ msgstr "Não é possível salvar o arquivo."

#~ msgid ""
#~ "Too many videos for this disc size.\n"
#~ "Please, select a bigger disc type or remove some videos."
#~ msgstr ""
#~ "Vídeos demais para este tamanho de disco.\n"
#~ "Por favor, escolha um tipo disco maior ou remova alguns vídeos."

#~ msgid ""
#~ "Some files weren't video files.\n"
#~ "None added."
#~ msgstr ""
#~ "Alguns arquivos não são vídeos.\n"
#~ "Nenhum adicionado."

#~ msgid ""
#~ "Your project contains %(X)d movie files, but the maximum is %(MAX)d. "
#~ "Please, remove some files and try again."
#~ msgstr ""
#~ "Seu projeto contém %(X)d arquivos de vídeo, mas o máximo é de %(MAX)d. "
#~ "Por favor, remova alguns arquivos e tente outra vez."

#~ msgid "no chapters"
#~ msgstr "sem capítulos"

#~ msgid "Unsaved disc structure"
#~ msgstr "Estrutura de disco ainda não salva."

#~ msgid "Codepage"
#~ msgstr "Codificação do texto"

#~ msgid "Language"
#~ msgstr "Idioma"

#~ msgid "File doesn't seem to be a video file."
#~ msgstr "O arquivo não parece ser um vídeo."

#~ msgid "Please, add only one file each time."
#~ msgstr "Por favor, adicione apenas um arquivo de cada vez."

#~ msgid "Please, add a movie file before adding subtitles."
#~ msgstr ""
#~ "Por favor, adicione um arquivo de vídeo antes de adicionar legendas."

#~ msgid ""
#~ "Conversion failed.\n"
#~ "It seems a bug of SPUMUX."
#~ msgstr ""
#~ "A conversão falhou.\n"
#~ "Parece ser um bug do SPUMUX."

#~ msgid ""
#~ "File copy failed\n"
#~ "Maybe you ran out of disk space?"
#~ msgstr ""
#~ "Falha ao copiar\n"
#~ "Há espaço livre suficiente no disco?"

#~ msgid "Creating preview"
#~ msgstr "Criando pré-visualização"

#~ msgid ""
#~ "Conversion failed.\n"
#~ "It seems a bug of Mencoder."
#~ msgstr ""
#~ "Conversão falhou.\n"
#~ "Parece ser um bug do Mencoder."

#~ msgid "Also check the extra params passed to Mencoder for syntax errors."
#~ msgstr ""
#~ "Verificar erros de sintaxe também nos parâmetros extras passados ao "
#~ "Mencoder."

#~ msgid ""
#~ "Conversion failed\n"
#~ "Maybe you ran out of disk space?"
#~ msgstr ""
#~ "Falha na conversão\n"
#~ "Pode não haver espaço livre suficiente no disco?"

#~ msgid "Failed to create the menues."
#~ msgstr "Falha ao criar os menus."

#~ msgid "Menu generation failed."
#~ msgstr "Geração do menu falhou."

#~ msgid ""
#~ "Can't add the buttons to the menus.\n"
#~ "It seems a bug of SPUMUX."
#~ msgstr ""
#~ "Não foi possível adicionar os botões aos menus.\n"
#~ "Parece ser um bug do SPUMUX."

#~ msgid "Erase temporary files"
#~ msgstr "Apagar arquivos temporários"

#~ msgid "Use optimizations for multicore CPUs"
#~ msgstr "Usar otimizações para CPUs de núcleo múltiplo"

#~ msgid "<b>Options</b>"
#~ msgstr "<b>Opções</b>"

#~ msgid "Temporary files folder:"
#~ msgstr "Diretório para arquivos temporários:"

#~ msgid "Folder for temporary file"
#~ msgstr "Diretório para arquivo temporário"

#, fuzzy
#~ msgid "<b>Folders</b>"
#~ msgstr "<b>Arquivos</b>"

#, fuzzy
#~ msgid "<b>Workarounds</b>"
#~ msgstr "<b>Rotação</b>"

#~ msgid "File:"
#~ msgstr "Arquivo:"

#~ msgid "Clear"
#~ msgstr "Limpar"

#~ msgid "Encoding:"
#~ msgstr "Codificação:"

#~ msgid "Language:"
#~ msgstr "Idioma:"

#~ msgid "Put subtitles upper"
#~ msgstr "Posicionar as legendas mais para cima"

#~ msgid "Add subtitles"
#~ msgstr "Adicionar legendas"

#~ msgid "Choose a file"
#~ msgstr "Escolha um arquivo"

#~ msgid "Aborted. There can be temporary files at the destination directory."
#~ msgstr ""
#~ "Cancelado. Ainda pode haver arquivos temporários no diretório de destino."

#~ msgid "Delete chapter"
#~ msgstr "Excluir capítulo"

#~ msgid ""
#~ "You chose to delete this chapter and\n"
#~ "reorder the other ones:"
#~ msgstr ""
#~ "Você optou por apagar este capítulo e\n"
#~ "reordenar os restantes:"

#~ msgid "Proceed?"
#~ msgstr "Continuar?"

#~ msgid "Delete subtitle"
#~ msgstr "Excluir legenda"

#~ msgid "Remove the selected subtitle file?"
#~ msgstr "Remover o arquivo de legenda selecionado?"

#~ msgid ""
#~ "You chose to delete this title and\n"
#~ "reorder the other ones:"
#~ msgstr ""
#~ "Você optou por apagar este título\n"
#~ "e reordenar os restantes:"

#~ msgid "Disc type selection"
#~ msgstr "Escolha do tipo de disco"

#~ msgid "<b>Choose the disc type you want to create with DeVeDe</b>"
#~ msgstr "<b>Selecione o tipo de disco que deseja criar com o DeVeDe</b>"

#~ msgid ""
#~ "<b>Video DVD</b>\n"
#~ "Creates a video DVD suitable for all DVD home players"
#~ msgstr ""
#~ "<b>DVD de Vídeo</b>\n"
#~ "Cria um DVD de vídeo compatível com todos os aparelhos caseiros"

#~ msgid ""
#~ "<b>VideoCD</b>\n"
#~ "Creates a VideoCD, with a picture quality equivalent to VHS"
#~ msgstr ""
#~ "<b>VideoCD</b>\n"
#~ "Cria um VideoCD, com qualidade de imagem equivalente ao VHS"

#~ msgid ""
#~ "<b>Super VideoCD</b>\n"
#~ "Creates a VideoCD with better picture quality"
#~ msgstr ""
#~ "<b>Super VideoCD</b>\n"
#~ "Cria um VideoCD com melhor qualidade de imagem"

#~ msgid ""
#~ "<b>China VideoDisc</b>\n"
#~ "Another kind of Super VideoCD"
#~ msgstr ""
#~ "<b>China VideoDisc</b>\n"
#~ "Outro tipo de Super VideoCD"

#~ msgid ""
#~ "<b>DivX / MPEG-4</b>\n"
#~ "Creates files compliant with DivX home players"
#~ msgstr ""
#~ "<b>DivX / MPEG-4</b>\n"
#~ "Cria arquivos compatíveis com aparelhos caseiros que suportam DivX"

#~ msgid ""
#~ "Your DVD structure contains empty titles.\n"
#~ "Continue anyway?"
#~ msgstr ""
#~ "Sua estrutura de DVD contém títulos vazios.\n"
#~ "Continuar mesmo assim?"

#~ msgid "Job done!"
#~ msgstr "Tarefa terminada!"

#~ msgid "Delete current disc structure?"
#~ msgstr "Excluir a estrutura de disco atual?"

#~ msgid "Delete the current disc structure and start again?"
#~ msgstr "Apagar a estrutura de disco atual e recomeçar?"

#~ msgid "File properties"
#~ msgstr "Propriedades do arquivo"

#~ msgid "<b>File</b>"
#~ msgstr "<b>Arquivo</b>"

#~ msgid "Frames per second:"
#~ msgstr "Quadros por segundo:"

#~ msgid "Original length (seconds):"
#~ msgstr "Duração original (segundos):"

#~ msgid "Original audio rate (Kbits/sec):"
#~ msgstr "Taxa de áudio original (Kbits/s):"

#~ msgid "Original video rate (Kbits/sec):"
#~ msgstr "Taxa de vídeo original (Kbits/s):"

#~ msgid "Original size (pixels):"
#~ msgstr "Tamanho original (pixels):"

#~ msgid "Final size (pixels):"
#~ msgstr "Tamanho final (pixels):"

#~ msgid "Estimated final length (MBytes):"
#~ msgstr "Duração final estimada (MBytes):"

#~ msgid "<b>File info</b>"
#~ msgstr "<b>Informações sobre o arquivo</b>"

#~ msgid "PAL/SECAM"
#~ msgstr "PAL/SECAM"

#~ msgid "NTSC"
#~ msgstr "NTSC"

#~ msgid "<b>Video format</b>"
#~ msgstr "<b>Formato de vídeo</b>"

#~ msgid "Audio track:"
#~ msgstr "Trilha de áudio:"

#~ msgid "This video file has no audio tracks."
#~ msgstr "Este arquivo de vídeo não inclui trilhas de áudio."

#~ msgid "<b>Audio tracks</b>"
#~ msgstr "<b>Trilhas de áudio</b>"

#, fuzzy
#~ msgid "<b>Volume (%)</b>"
#~ msgstr "<b>Arquivo</b>"

#~ msgid "Store full length"
#~ msgstr "Duração completa"

#~ msgid "Store the first half"
#~ msgstr "Primeira metade"

#~ msgid "Store the second half"
#~ msgstr "Segunda metade"

#~ msgid "<b>File split</b>"
#~ msgstr "<b>Divisão de arquivo</b>"

#~ msgid "Font size:"
#~ msgstr "Tamanho da Fonte:"

#, fuzzy
#~ msgid "Force subtitles"
#~ msgstr "Adicionar legendas"

#~ msgid "DivX format doesn't support subtitles. Please, read the FAQ."
#~ msgstr "O formato DivX não suporta legendas. Por favor, leia o FAQ."

#~ msgid "<b>Subtitles</b>"
#~ msgstr "<b>Legendas</b>"

#~ msgid "<b>Video rate (Kbits/sec)</b>"
#~ msgstr "<b>Taxa de vídeo (Kbits/s)</b>"

#~ msgid "<b>Audio rate (Kbits/sec)</b>"
#~ msgstr "<b>Taxa de áudio (Kbits/s)</b>"

#~ msgid "Split the file in chapters (for easy seeking)"
#~ msgstr "Dividir o arquivo em capítulos (para fácil localização de trechos)"

#~ msgid "Size (in minutes) for each chapter:"
#~ msgstr "Tamanho (em minutos) para cada capítulo:"

#~ msgid "<b>Division in chapters</b>"
#~ msgstr "<b>Divisão em capítulos</b>"

#~ msgid "General"
#~ msgstr "Geral"

#~ msgid "352x240"
#~ msgstr "352x240"

#~ msgid "Default"
#~ msgstr "Padrão"

#~ msgid "352x480"
#~ msgstr "352x480"

#~ msgid "480x480"
#~ msgstr "480x480"

#~ msgid "720x480"
#~ msgstr "720x480"

#~ msgid "704x480"
#~ msgstr "704x480"

#~ msgid "1920x1080"
#~ msgstr "1920x1080"

#~ msgid "1280x720"
#~ msgstr "1280x720"

#~ msgid "160x128"
#~ msgstr "160x128"

#~ msgid "<b>Final size</b>"
#~ msgstr "<b>Tamanho final</b>"

#~ msgid "4:3"
#~ msgstr "4:3"

#~ msgid "16:9"
#~ msgstr "16:9"

#~ msgid "<b>Aspect ratio</b>"
#~ msgstr "<b>Relação de aspecto</b>"

#~ msgid "Video format"
#~ msgstr "Formato de vídeo"

#~ msgid "No rotation"
#~ msgstr "Sem rotação"

#~ msgid "Rotate 180 degrees"
#~ msgstr "Girar 180 graus"

#~ msgid "Rotate 90 degrees clockwise"
#~ msgstr "Girar 90 graus horários"

#~ msgid "Rotate 90 degrees counter-clockwise"
#~ msgstr "Girar 90 graus anti-horários"

#~ msgid "<b>Rotation</b>"
#~ msgstr "<b>Rotação</b>"

#~ msgid "Horizontal mirror"
#~ msgstr "Espelhar horizontalmente"

#~ msgid "Vertical mirror"
#~ msgstr "Espelhar verticalmente"

#~ msgid "<b>Mirror</b>"
#~ msgstr "<b>Espelhado</b>"

#~ msgid "Swap fields"
#~ msgstr "Inverter campos"

#~ msgid "<b>Field order</b>"
#~ msgstr "<b>Ordem do campo</b>"

#~ msgid "Add black bars"
#~ msgstr "Adicionar barras pretas"

#~ msgid "Scale picture"
#~ msgstr "Redimensionar quadro"

#~ msgid "<b>Scaling mode</b>"
#~ msgstr "<b>Redimensionamento</b>"

#~ msgid "Video options"
#~ msgstr "Opções de vídeo"

#~ msgid "Use MBCMP (faster)"
#~ msgstr "Usar MBCMP (mais rápido)"

#~ msgid "Select MBD mode which needs the fewest bits"
#~ msgstr "Selecionar o modo MDB que necessitar de menos bits"

#~ msgid ""
#~ "Select the MBD mode which has the best rate distortion (better quality)"
#~ msgstr ""
#~ "Selecionar o modo MDB que tiver a melhor taxa de distorção (melhor "
#~ "qualidade)"

#~ msgid ""
#~ "Use Trellis Searched Quantization (better quality, slower conversion)"
#~ msgstr ""
#~ "Usar Trellis Searched Quantization (melhor qualidade, conversão mais "
#~ "lenta)"

#, fuzzy
#~ msgid ""
#~ "<b>MacroBlock decision algorithm and Trellis searched quantization</b>"
#~ msgstr "<b>Algoritmo de seleção do MacroBloco</b>"

#, fuzzy
#~ msgid "Use dual pass encoding (better quality, much slower conversion)"
#~ msgstr ""
#~ "Usar Trellis Searched Quantization (melhor qualidade, conversão mais "
#~ "lenta)"

#~ msgid "Median deinterlacing filter"
#~ msgstr "Filtro de desentrelaçamento mediano"

#~ msgid "Don't deinterlace"
#~ msgstr "Não desentrelaçar"

#~ msgid "YADIF deinterlacing filter"
#~ msgstr "Filtro de desentrelaçamento YADIF"

#~ msgid "FFMpeg deinterlacing filter"
#~ msgstr "Filtro de desentrelaçamento do FFMpeg"

#~ msgid "Linear blend"
#~ msgstr "Mesclagem linear"

#~ msgid "Lowpass5 deinterlacing filter"
#~ msgstr "Filtro de desentrelaçamento Lowpass5"

#~ msgid "<b>Deinterlacing</b>"
#~ msgstr "<b>Desentrelaçamento</b>"

#~ msgid "Quality"
#~ msgstr "Qualidade"

#~ msgid "Audio delay (in seconds):"
#~ msgstr "Atraso de áudio (em segundos):"

#~ msgid "Create DVD with 5.1 channel sound"
#~ msgstr "Criar DVD com som 5.1"

#~ msgid ""
#~ "This file already has AC3 sound (copy audio data instead of recompress it)"
#~ msgstr ""
#~ "O arquivo já tem o som no formato AC3 (copiar o áudio ao invés de "
#~ "recodificá-lo)"

#~ msgid "<b>Audio</b>"
#~ msgstr "<b>Áudio</b>"

#~ msgid "Audio"
#~ msgstr "Áudio"

#~ msgid "This file is already a DVD/xCD-suitable MPEG-PS file"
#~ msgstr "Este arquivo já está em formato MPEG-PS, pronto para DVD/xCD."

#~ msgid "Repack audio and video without reencoding (useful for VOB files)"
#~ msgstr ""
#~ "Reempacotar áudio e vídeo sem recodificação (útil para arquivos VOB)"

#~ msgid "Use a GOP of 12 frames (improves compatibility)"
#~ msgstr "Usar um GOP de 12 quadros (aumenta a compatibilidade)"

#~ msgid "<b>Special</b>"
#~ msgstr "<b>Especial</b>"

#~ msgid "Main (-X -Y)"
#~ msgstr "Main (-X -Y)"

#~ msgid "-VF (X,Y)"
#~ msgstr "-VF (X,Y)"

#~ msgid "-LAVCOPTS (X:Y)"
#~ msgstr "-LAVCOPTS (X:Y)"

#~ msgid "-LAMEOPTS (X:Y)"
#~ msgstr "-LAMEOPTS (X:Y)"

#, fuzzy
#~ msgid "<b>Extra parameters for coder</b>"
#~ msgstr "<b>Parâmetros adicionais para o Mencoder</b>"

#~ msgid "Misc"
#~ msgstr "Miscelânea"

#~ msgid "Advanced options"
#~ msgstr "Opções avançadas"

#~ msgid "Preview"
#~ msgstr "Pré-visualização"

#, fuzzy
#~ msgid ""
#~ "Choose the folder where DeVeDe will create the DVD image and a name for "
#~ "it. Don't use a folder in a VFAT/FAT32 drive. Characters /, | and \\ are "
#~ "not allowed and will be replaced by underscores."
#~ msgstr ""
#~ "Selecione o diretório onde DeVeDe criará a imagem de DVD, assim como o "
#~ "nome desta. Não use um diretório de uma unidade do tipo VFAT/FAT32."

#~ msgid "Choose a folder"
#~ msgstr "Selecione um diretório"

#~ msgid "Load a disc structure"
#~ msgstr "Carregar uma estrutura de disco"

#~ msgid "Warning: you already have a disc structure"
#~ msgstr "Aviso: você já tem uma estrutura de disco"

#~ msgid ""
#~ "If you load a disc structure you will loose your current structure "
#~ "(unless you saved it). Continue?"
#~ msgstr ""
#~ "Se você carregar uma estrutura de disco a atual será perdida (a menos que "
#~ "já tenha sido salva). Deseja continuar?"

#~ msgid "185 MB CD"
#~ msgstr "CD de 185 MB"

#~ msgid "650 MB CD"
#~ msgstr "CD de 650 MB"

#~ msgid "700 MB CD"
#~ msgstr "CD de 700 MB"

#~ msgid "1.4 GB DVD"
#~ msgstr "DVD de 1.4 GB"

#~ msgid "4.7 GB DVD"
#~ msgstr "DVD de 4.7 GB"

#~ msgid "8.5 GB DVD"
#~ msgstr "DVD de 8.5 GB"

#~ msgid "DeVeDe"
#~ msgstr "DeVeDe"

#~ msgid "_File"
#~ msgstr "_Arquivo"

#~ msgid "_Help"
#~ msgstr "A_juda"

#~ msgid "<b>Titles</b>"
#~ msgstr "<b>Títulos</b>"

#~ msgid "<b>Files</b>"
#~ msgstr "<b>Arquivos</b>"

#~ msgid "Length (seconds):"
#~ msgstr "Duração (segundos):"

#~ msgid "Video rate (Kbits/sec):"
#~ msgstr "Taxa de vídeo (Kbits/s):"

#~ msgid "Audio rate (Kbits/sec):"
#~ msgstr "Taxa de áudio (Kbits/s):"

#~ msgid "Estimated length (MBytes):"
#~ msgstr "Duração estimada (MBytes):"

#~ msgid "Output aspect ratio:"
#~ msgstr "Relação de aspecto da saída:"

#~ msgid "Size of chapters:"
#~ msgstr "Tamanho dos capítulos:"

#~ msgid "Adjust disc usage"
#~ msgstr "Ajustar ao tamanho do disco"

#~ msgid "0.0%"
#~ msgstr "0,0%"

#~ msgid "Media size:"
#~ msgstr "Tamanho da mídia:"

#~ msgid "<b>Disc usage</b>"
#~ msgstr "<b>Uso do disco</b>"

#~ msgid "PAL"
#~ msgstr "PAL"

#~ msgid "<b>Default format</b>"
#~ msgstr "<b>Formato padrão</b>"

#, fuzzy
#~ msgid "Create a menu with the titles"
#~ msgstr "Adicionar um menu com os títulos"

#~ msgid "Menu options"
#~ msgstr "Opções de menu"

#~ msgid "Preview menu"
#~ msgstr "Pré-visualização do menu"

#~ msgid "<b>Menus</b>"
#~ msgstr "<b>Menus</b>"

#~ msgid "Only convert film files to compliant MPEG files"
#~ msgstr "Apenas converter os vídeos para arquivos compatíveis com MPEG"

#~ msgid "Create disc structure"
#~ msgstr "Criar estrutura de disco"

#~ msgid "Create an ISO or BIN/CUE image, ready to burn to a disc"
#~ msgstr "Criar uma imagem ISO ou BIN/CUE pronta para gravar na mídia"

#~ msgid "<b>Action</b>"
#~ msgstr "<b>Ação</b>"

#~ msgid "Menu preview"
#~ msgstr "Pré-visualização do menu"

#~ msgid "Shadow color:"
#~ msgstr "Cor da sombra:"

#~ msgid "Text color:"
#~ msgstr "Cor do texto:"

#~ msgid "Text:"
#~ msgstr "Texto:"

#~ msgid "Font:"
#~ msgstr "Fonte:"

#~ msgid "<b>Menu title</b>"
#~ msgstr "<b>Título do menu</b>"

#~ msgid "Select a picture to be used as background for disc's menu"
#~ msgstr ""
#~ "Selecione uma imagem para ser usada como plano de fundo no menu do disco"

#~ msgid "Set default background"
#~ msgstr "Definir como plano de fundo padrão"

#~ msgid "<b>Menu background (only PNG)</b>"
#~ msgstr "<b>Plano de fundo do menu (somente PNG)</b>"

#, fuzzy
#~ msgid "Choose a music file"
#~ msgstr "Escolha um arquivo"

#~ msgid "Set menus without sound"
#~ msgstr "Definir menus sem som."

#~ msgid "<b>Menu music</b>"
#~ msgstr "<b>Música de menu</b>"

#, fuzzy
#~ msgid "<b>Horizontal</b>"
#~ msgstr "<b>Alinhamento horizontal</b>"

#, fuzzy
#~ msgid "<b>Vertical</b>"
#~ msgstr "<b>Especial</b>"

#~ msgid "Top"
#~ msgstr "Superior"

#~ msgid "Middle"
#~ msgstr "Meio"

#~ msgid "Bottom"
#~ msgstr "Inferior"

#~ msgid "Left"
#~ msgstr "Esquerda"

#~ msgid "Center"
#~ msgstr "Centro"

#~ msgid "Right"
#~ msgstr "Direita"

#, fuzzy
#~ msgid "<b>Margins (%)</b>"
#~ msgstr "<b>Menus</b>"

#~ msgid "<b>Menu position</b>"
#~ msgstr "<b>Posição do menu</b>"

#~ msgid "Color for unselected titles:"
#~ msgstr "Cor para os títulos não selecionados:"

#~ msgid "Color for shadows:"
#~ msgstr "Cor para as sombras:"

#~ msgid "Color for selected title:"
#~ msgstr "Cor para o título selecionado:"

#~ msgid "Background color for titles:"
#~ msgstr "Cor de fundo para os títulos:"

#~ msgid "<b>Menu font and colors</b>"
#~ msgstr "<b>Cores e fonte do menu</b>"

#~ msgid "Show menu at disc startup"
#~ msgstr "Mostrar menu no início da reprodução"

#~ msgid "Jump to the first title at startup"
#~ msgstr "Pular para o primeiro título ao iniciar a reprodução."

#~ msgid "<b>Disc startup options</b>"
#~ msgstr "<b>Opções de início de reprodução</b>"

#~ msgid "Can't find the font for subtitles. Aborting."
#~ msgstr "Não foi possível encontrar a fonte para as legendas. Cancelando."

#~ msgid "Replay the preview again?"
#~ msgstr "Rever a pré-visualização?"

#~ msgid "<b>Preview video</b>"
#~ msgstr "<b>Pré-visualizar Vídeo</b>"

#~ msgid ""
#~ "DeVeDe will create a preview with the selected parameters, so you will be "
#~ "able to check video quality, audio sync and so on."
#~ msgstr ""
#~ "O DeVeDe criará uma pré-visualização com os parâmetros selecionados, "
#~ "então será possível examinar a qualidade do vídeo, sincronismo do áudio "
#~ "etc."

#~ msgid "Preview length:"
#~ msgstr "Duração da pré-visualização:"

#~ msgid "Can't find the following programs:"
#~ msgstr "Os seguintes programas não foram encontrados:"

#~ msgid ""
#~ "DeVeDe needs them in order to work. If you want to use DeVeDe, you must "
#~ "install all of them."
#~ msgstr ""
#~ "O DeVeDe precisa deles para funcionar. Se quiser usar o DeVeDe é "
#~ "necessário instalar todos."

#~ msgid "Creating..."
#~ msgstr "Criando…"

#~ msgid "Save current disc structure"
#~ msgstr "Salvar estrutura de disco atual"

#~ msgid "Title properties"
#~ msgstr "Propriedades do título"

#~ msgid "<b>Title's name</b>"
#~ msgstr "<b>Nome do título</b>"

#~ msgid "Stop reproduction/show disc menu"
#~ msgstr "Parar reprodução/mostrar menu do disco"

#~ msgid "Play the first title"
#~ msgstr "Reproduzir o primeiro título"

#~ msgid "Play the previous title"
#~ msgstr "Reproduzir o título anterior"

#~ msgid "Play this title again (loop)"
#~ msgstr "Reproduzir este título novamente (loop)"

#~ msgid "Play the next title"
#~ msgstr "Reproduzir o próximo título"

#~ msgid "Play the last title"
#~ msgstr "Reproduzir o último título"

#~ msgid "<b>Action to perform when this title ends</b>"
#~ msgstr "<b>Ação ao término deste título</b>"

#~ msgid "Warning"
#~ msgstr "Aviso"

#~ msgid "<b>Vertical alignment</b>"
#~ msgstr "<b>Alinhamento vertical</b>"

#~ msgid "Converting files from title"
#~ msgstr "Convertendo arquivos do título "

#~ msgid "gtk-cancel"
#~ msgstr "gtk-cancel"

#~ msgid "gtk-ok"
#~ msgstr "gtk-ok"

#~ msgid "<b>Trellis searched quantization</b>"
#~ msgstr "<b>Trellis searched quantization</b>"

#~ msgid "gtk-no"
#~ msgstr "gtk-no"

#~ msgid "gtk-yes"
#~ msgstr "gtk-yes"

#~ msgid "gtk-add"
#~ msgstr "gtk-add"

#~ msgid "gtk-help"
#~ msgstr "gtk-help"

#~ msgid "gtk-remove"
#~ msgstr "gtk-remove"

#~ msgid "gtk-go-back"
#~ msgstr "gtk-go-back"

#~ msgid "gtk-go-forward"
#~ msgstr "gtk-go-forward"

#~ msgid "gtk-open"
#~ msgstr "gtk-open"

#~ msgid ""
#~ "Uses multiple threads with Mencoder, allowing a faster compression "
#~ "process. But requires Mencoder 1.0 RC2 or later."
#~ msgstr ""
#~ "Usar múltiplas linhas de execução com o Mencoder, permitindo um processo "
#~ "de compressão mais rápido. Requer o Mencoder versão 1.0 RC2 ou posterior."

#~ msgid "gtk-about"
#~ msgstr "gtk-about"

#~ msgid "gtk-go-down"
#~ msgstr "gtk-go-down"

#~ msgid "gtk-go-up"
#~ msgstr "gtk-go-up"

#~ msgid "gtk-new"
#~ msgstr "gtk-new"

#~ msgid "gtk-properties"
#~ msgstr "gtk-properties"

#~ msgid "gtk-quit"
#~ msgstr "gtk-quit"

#~ msgid "gtk-save"
#~ msgstr "gtk-save"

#~ msgid "gtk-save-as"
#~ msgstr "gtk-save-as"

#~ msgid "CVD"
#~ msgstr "CVD"

#~ msgid "DIVX / MPEG-4 ASP"
#~ msgstr "DIVX / MPEG-4 ASP"

#~ msgid "Super VideoCD"
#~ msgstr "Super VideoCD"

#~ msgid "Video DVD"
#~ msgstr "DVD de Vídeo"

#~ msgid "VideoCD"
#~ msgstr "VideoCD"

#~ msgid ""
#~ "<b>Preview video</b>\n"
#~ "\n"
#~ "DeVeDe will create a preview with the selected parameters, so you will be "
#~ "able to check video quality, audio sync and so on.\n"
#~ "\n"
#~ "Choose how many seconds of film you want to convert to create the "
#~ "preview, and the directory where you want to store the temporary file "
#~ "(default is /var/tmp)."
#~ msgstr ""
#~ "<b>Pré-visualizar vídeo</b>\n"
#~ "\n"
#~ "DeVeDe criará uma pré-visualização com os parâmetros selecionados, "
#~ "possibilitando a verificação da qualidade do vídeo, sincronismo do áudio "
#~ "e etc.\n"
#~ "\n"
#~ "Selecione quantos segundos de filme serão convertidos para a pré-"
#~ "visualização e a pasta onde deseja criar o arquivo temporário (o padrão "
#~ "é /var/tmp)."

#~ msgid ""
#~ "A CD with video in MPEG-1 format, with quality equivalent to VHS. Stores "
#~ "up to 80 minutes in a 80-minutes CD-R. Playable in all DVD players."
#~ msgstr ""
#~ "Um CD com vídeo em formato MPEG-1, com qualidade equivalente ao VHS. "
#~ "Armazena até 80 minutos em um CD-R de 700 MB. Reproduz em todos os DVD "
#~ "players."

#~ msgid ""
#~ "A CD with video in MPEG-2 format and a resolution of 352x480 (or 352x576 "
#~ "in PAL). Playable in most of DVD players."
#~ msgstr ""
#~ "Um CD com vídeo no formato MPEG-2 e uma resolução de 352x480 (ou 352x576 "
#~ "em PAL). Funciona na maioria dos DVD players."

#~ msgid ""
#~ "A CD with video in MPEG-2 format and a resolution of 480x480 pixels "
#~ "(480x576 in PAL). Playable in nearly all DVD players."
#~ msgstr ""
#~ "Um CD com vídeo em formato MPEG 2 e uma resolução de 480x480 pixels "
#~ "(480x576 em PAL). Reproduz em quase todos os DVD players."

#~ msgid "A classic Video DVD, playable in all DVD players."
#~ msgstr "Um DVD de Vídeo clássico, reproduz em qualquer DVD player."

#~ msgid "Output video format:"
#~ msgstr "Formato de saída de vídeo:"

#~ msgid "There was an error while importing the file:"
#~ msgstr "Ocorreu um erro durante a importação do arquivo:"

#~ msgid "Audio options"
#~ msgstr "Opções de áudio"

#~ msgid "Menu properties"
#~ msgstr "Propriedades do menu"

#~ msgid "Video (1)"
#~ msgstr "Vídeo (1)"

#~ msgid "Video (2)"
#~ msgstr "Vídeo (2)"

#, fuzzy
#~ msgid ""
#~ "Failed to write IMAGE to the destination directory.\n"
#~ "Check that you have privileges and free space there."
#~ msgstr ""
#~ "Falha ao gravar no diretório de destino.\n"
#~ "Verifique se há permissões e espaço livre suficientes."

#, fuzzy
#~ msgid ""
#~ "Failed to write HIGHLIGHTto the destination directory.\n"
#~ "Check that you have privileges and free space there."
#~ msgstr ""
#~ "Falha ao gravar no diretório de destino.\n"
#~ "Verifique se há permissões e espaço livre suficientes."

#, fuzzy
#~ msgid ""
#~ "Failed to write SELECT to the destination directory.\n"
#~ "Check that you have privileges and free space there."
#~ msgstr ""
#~ "Falha ao gravar no diretório de destino.\n"
#~ "Verifique se há permissões e espaço livre suficientes."

#~ msgid ""
#~ "Can't reduce menu colors.\n"
#~ "Check that you have ImageMagic's Convert installed"
#~ msgstr ""
#~ "Não foi possível reduzir as cores do menu.\n"
#~ "Verifique se o Convert do ImageMagic está instalado."

#, fuzzy
#~ msgid "<b>Menu appearance</b>"
#~ msgstr "<b>Formato do menu</b>"

#~ msgid "<b>File with subtitles</b>"
#~ msgstr "<b>Arquivo com legendas</b>"

#~ msgid "Choose the disc to create"
#~ msgstr "Escolha o tipo de disco a ser criado"

#~ msgid "Use 16:9 aspect ratio for output"
#~ msgstr "Usar a proporção 16:9 para a saída"

#~ msgid "Choose a file with a movie"
#~ msgstr "Selecione um arquivo com um filme"

#~ msgid "That file contains a structure for a different kind of disc"
#~ msgstr "Este arquivo contém uma estrutura para um outro tipo de disco"

#~ msgid "Please, open DeVeDe again with the right disc type"
#~ msgstr "Por favor, reabra o DeVeDe com a opção de tipo de disco correta"

#~ msgid "Elapsed time:"
#~ msgstr "Tempo decorrido:"

#~ msgid "Aborted"
#~ msgstr "Cancelado"

#~ msgid "Create a preview"
#~ msgstr "Criar uma pré-visualização"

#~ msgid "DeVeDe: Cancel job?"
#~ msgstr "DeVeDe: Cancelar tarefa?"

#~ msgid "Destination folder"
#~ msgstr "Diretório de destino"

#~ msgid "Done"
#~ msgstr "Pronto"

#~ msgid "Replay preview"
#~ msgstr "Rever pré-visualização"

#~ msgid "Warning: empty titles"
#~ msgstr "Aviso: títulos vazios"

#~ msgid "2007 Raster Software Vigo"
#~ msgstr "2007 Raster Software Vigo"

#~ msgid ""
#~ "Martin Sin\n"
#~ "Maurizio Daniele\n"
#~ "Marco de Freitas\n"
#~ "Lars-Erik Aunevik Labori\n"
#~ "Hagen Hoepfner\n"
#~ "Joel Calado\n"
#~ "Patrick Monnerat"
#~ msgstr ""
#~ "Martin Sin\n"
#~ "Maurizio Daniele\n"
#~ "Marco de Freitas\n"
#~ "Lars-Erik Aunevik Labori\n"
#~ "Hagen Hoepfner\n"
#~ "Joel Calado\n"
#~ "Patrick Monnerat"

#~ msgid "total"
#~ msgstr "total"

#~ msgid "translator-credits"
#~ msgstr "Marco de Freitas <marcodefreitas@gmail.com>, 2006"
