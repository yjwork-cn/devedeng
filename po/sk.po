# Slovak translation of DeVeDe.
# Copyright (C) 2006 THE devede'S COPYRIGHT HOLDER
# This file is distributed under the same license as the DeVeDe package.
# Jozef Riha <jose1711@gmail.com>, 2007, 2008, 2009, 2010.
# Dušan Kazik <prescott66@gmail.com>, 2015, 2016.
# Sergio Costas <rastersoft@gmail.com>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: devede 3.8\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-13 22:08+0200\n"
"PO-Revision-Date: 2016-04-30 11:42+0200\n"
"Last-Translator: Dušan Kazik <prescott66@gmail.com>\n"
"Language-Team: Español; Castellano <rastersoft@gmail.com>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../src/devedeng/add_files.py:44
msgid "Video files"
msgstr "Súbory videí"

#: ../src/devedeng/add_files.py:49 ../src/devedeng/ask_subtitles.py:92
#: ../src/devedeng/dvd_menu.py:146 ../src/devedeng/dvd_menu.py:158
#: ../src/devedeng/opensave.py:48
msgid "All files"
msgstr "Všetky súbory"

#: ../src/devedeng/ask_subtitles.py:81
msgid "Subtitle files"
msgstr "Súbory s titulkami"

#: ../src/devedeng/avconv.py:114 ../src/devedeng/ffmpeg.py:114
#, python-format
msgid "Converting %(X)s (pass 2)"
msgstr "Konvertuje sa %(X)s (prechod 2)"

#: ../src/devedeng/avconv.py:117 ../src/devedeng/ffmpeg.py:117
#, python-format
msgid "Converting %(X)s (pass 1)"
msgstr "Konvertuje sa %(X)s (prechod 1)"

#: ../src/devedeng/avconv.py:127
#, python-format
msgid "Converting %(X)s"
msgstr "Konvertuje sa %(X)s"

#: ../src/devedeng/avconv.py:507
#, python-format
msgid "Creating menu %(X)d"
msgstr "Vytvára sa ponuka %(X)d"

#: ../src/devedeng/choose_disc_type.py:156
#: ../src/devedeng/choose_disc_type.py:167
#: ../src/devedeng/choose_disc_type.py:183
#: ../src/devedeng/choose_disc_type.py:194
#: ../src/devedeng/choose_disc_type.py:205
#: ../src/devedeng/choose_disc_type.py:215
#: ../src/devedeng/choose_disc_type.py:221
#: ../src/devedeng/choose_disc_type.py:227
#, python-format
msgid "\t%(program_name)s (not installed)\n"
msgstr "\t%(program_name)s (not installed)\n"

#: ../src/devedeng/choose_disc_type.py:159
#: ../src/devedeng/choose_disc_type.py:170
#: ../src/devedeng/choose_disc_type.py:186
#: ../src/devedeng/choose_disc_type.py:197
#: ../src/devedeng/choose_disc_type.py:208
#: ../src/devedeng/choose_disc_type.py:218
#: ../src/devedeng/choose_disc_type.py:224
#: ../src/devedeng/choose_disc_type.py:230
#, python-format
msgid "\t%(program_name)s (installed)\n"
msgstr "\t%(program_name)s (nainštalovaný)\n"

#: ../src/devedeng/choose_disc_type.py:161
#, python-format
msgid ""
"Movie identifiers (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""
"Programy na identifikáciu videí (nainštalujte aspoň jeden z týchto):\n"
"\n"
"%(program_list)s\n"

#: ../src/devedeng/choose_disc_type.py:172
#, python-format
msgid ""
"Movie players (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""
"Programy na prehrávanie videí(nainštalujte aspoň jeden z týchto):\n"
"\n"
"%(program_list)s\n"

#: ../src/devedeng/choose_disc_type.py:188
#, python-format
msgid ""
"Movie Converters (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""
"Programy na konverziu videí(nainštalujte aspoň jeden z týchto):\n"
"\n"
"%(program_list)s\n"

#: ../src/devedeng/choose_disc_type.py:200
#, python-format
msgid ""
"CD/DVD burners (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""
"Programy na vypaľovanie diskov CD/DVD(nainštalujte aspoň jeden z týchto):\n"
"\n"
"%(program_list)s\n"

#: ../src/devedeng/choose_disc_type.py:210
#, python-format
msgid ""
"ISO creators (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""
"Programy na vytvorenie obrazov ISO(nainštalujte aspoň jeden z týchto):\n"
"\n"
"%(program_list)s\n"

#: ../src/devedeng/choose_disc_type.py:233
#, python-format
msgid ""
"Other programs:\n"
"\n"
"%(program_list)s\n"
msgstr ""
"Ostatné programy:\n"
"\n"
"%(program_list)s\n"

#: ../src/devedeng/dvdauthor_converter.py:48
msgid "Creating DVD structure"
msgstr "Vytvára sa štruktúra disku DVD"

#: ../src/devedeng/dvd_menu.py:51 ../src/devedeng/dvd_menu.py:418
msgid "Play all"
msgstr ""

#: ../src/devedeng/dvd_menu.py:93
msgid "The selected file is a video, not an audio file"
msgstr "Vybratý súbor je súborom videa a nie zvuku"

#: ../src/devedeng/dvd_menu.py:93 ../src/devedeng/dvd_menu.py:96
msgid "Error"
msgstr "Chyba"

#: ../src/devedeng/dvd_menu.py:96
msgid "The selected file is not an audio file"
msgstr "Vybratý súbor nie je zvukovým súborom"

#: ../src/devedeng/dvd_menu.py:141
msgid "Picture files"
msgstr "Súbory obrázkov"

#: ../src/devedeng/dvd_menu.py:153
msgid "Sound files"
msgstr "Zvukové súbory"

#: ../src/devedeng/dvd_menu.py:409
#, python-brace-format
msgid "Page {0} of {1}"
msgstr ""

#: ../src/devedeng/dvd_menu.py:416
msgid "Play_all"
msgstr ""

#: ../src/devedeng/file_copy.py:31
#, python-format
msgid "Copying file %(X)s"
msgstr "Kopíruje sa súbor %(X)s"

#: ../src/devedeng/genisoimage.py:73 ../src/devedeng/mkisofs.py:73
msgid "Creating ISO image"
msgstr "Vytvára sa obraz ISO"

#: ../src/devedeng/help.py:37
msgid "Can't open the help files."
msgstr "Nedajú sa otvoriť súbory pomocníka."

#: ../src/devedeng/mux_dvd_menu.py:34
#, python-format
msgid "Mixing menu %(X)d"
msgstr "Mieša sa ponuka %(X)d"

#: ../src/devedeng/opensave.py:44
msgid "DevedeNG projects"
msgstr "Projekty DevedeNG"

#: ../src/devedeng/project.py:231
msgid "Abort the current DVD and exit?"
msgstr "Prerušiť tvorbu aktuálneho DVD a skončiť?"

#: ../src/devedeng/project.py:231
msgid "Exit DeVeDe"
msgstr "Ukončenie programu DeVeDe"

#: ../src/devedeng/project.py:285 ../src/devedeng/project.py:738
msgid "In menu"
msgstr ""

#: ../src/devedeng/project.py:287
msgid "The following files could not be added:"
msgstr "Nasledovné súbory sa nepodarilo pridať:"

#: ../src/devedeng/project.py:288 ../src/devedeng/project.py:746
msgid "Error while adding files"
msgstr "Chyba počas pridávania súborov"

#: ../src/devedeng/project.py:299
#, python-format
msgid "The file <b>%(X)s</b> <i>(%(Y)s)</i> will be removed."
msgstr "Súbor <b>%(X)s</b> <i>(%(Y)s)</i> bude odstránený."

#: ../src/devedeng/project.py:299
msgid "Delete file"
msgstr "Odstránenie súboru"

#: ../src/devedeng/project.py:499
#, python-format
msgid "The limit for this format is %(l)d files, but your project has %(h)d."
msgstr ""
"Tento formát ma obmedzený počet súborov na %(l)d, ale váš projekt obsahuje "
"%(h)d súborov."

#: ../src/devedeng/project.py:499
msgid "Too many files in the project"
msgstr "Príliš veľa súborov v projekte"

#: ../src/devedeng/project.py:508
#, python-format
msgid ""
"The selected folder already exists. To create the project, Devede must "
"delete it.\n"
"If you continue, the folder\n"
"\n"
" <b>%s</b>\n"
"\n"
" and all its contents <b>will be deleted</b>. Continue?"
msgstr ""
"Vybratý priečinok už existuje. Na vytvorenie projektu, ho musí program "
"Devede odstrániť.\n"
"Ak budete pokračovať, priečinok\n"
"\n"
" <b>%s</b>\n"
"\n"
" a všetok jeho obsah <b>bude ostránený</b>. Chcete pokračovať?"

#: ../src/devedeng/project.py:508
msgid "Delete folder"
msgstr "Odstránenie priečinka"

#: ../src/devedeng/project.py:643
msgid "Close current project and start a fresh one?"
msgstr "Zavrieť aktuálny projekt a začať nový?"

#: ../src/devedeng/project.py:643
msgid "New project"
msgstr "Nový projekt"

#: ../src/devedeng/project.py:678
msgid "The file already exists. Overwrite it?"
msgstr "Súbor už existuje. Chcete ho prepísať?"

#: ../src/devedeng/project.py:678
msgid "The file already exists"
msgstr "Súbor už existuje"

#: ../src/devedeng/project.py:744 ../src/devedeng/project.py:766
msgid "Page jump"
msgstr ""

#: ../src/devedeng/project.py:746
msgid "The following files in the project could not be added again:"
msgstr "Nasledovné súbory v projekte sa nepodarilo znovu pridať:"

#: ../src/devedeng/runner.py:90 ../src/devedeng/runner.py:91
msgid "Cancel the current job?"
msgstr "Zrušiť aktuálnu úlohu?"

#: ../src/devedeng/settings.py:45 ../src/devedeng/settings.py:63
msgid "Use all cores"
msgstr "Použiť všetky jadrá"

#: ../src/devedeng/settings.py:50
#, python-format
msgid "Use %(X)d core"
msgid_plural "Use %(X)d cores"
msgstr[0] "Použiť %(X)d jadro"
msgstr[1] "Použiť %(X)d jadrá"

#: ../src/devedeng/settings.py:57
#, python-format
msgid "Use all except %(X)d core"
msgid_plural "Use all except %(X)d cores"
msgstr[0] "Použiť všetky okrem %(X)d jadra"
msgstr[1] "Použiť všetky okrem %(X)d jadier"

#: ../src/devedeng/settings.py:128
msgid "No discs supported"
msgstr "Žiadne podporované disky"

#: ../src/devedeng/subtitles_mux.py:43
#, python-format
msgid "Adding %(L)s subtitles to %(X)s"
msgstr "Pridávajú sa titulky jazyka %(L)s do %(X)s"

#: ../src/devedeng/title.py:32
#, python-format
msgid "Title %(X)d"
msgstr "Titul %(X)d"

#: ../src/devedeng/vcdimager_converter.py:48
msgid "Creating CD image"
msgstr "Vytvára sa obraz disku CD"

#~ msgid "Page %(X)d of %(Y)d"
#~ msgstr "Stránka %(X)d z %(Y)d"

#~ msgid "2014 Raster Software Vigo"
#~ msgstr "2014 Raster Software Vigo"

#~ msgid "Add new file(s)"
#~ msgstr "Pridanie nových súborov"

#~ msgid "File:"
#~ msgstr "Súbor:"

#~ msgid "Encoding:"
#~ msgstr "Kódovánie:"

#~ msgid "Language:"
#~ msgstr "Jazyk:"

#~ msgid "Put subtitles upper"
#~ msgstr "Vložiť titulky hore"

#~ msgid "devede"
#~ msgstr "devede"

#~ msgid ""
#~ "Choose the folder where Devede will create the files and a name for it. "
#~ "Do not use a folder in a VFAT/FAT32 drive. Characters /, | and \\ are not "
#~ "accepted and will be replaced by underscores."
#~ msgstr ""
#~ "Zvoľte priečinok, kde má program Devede vytvoriť súbory a jeho názov. "
#~ "Nepoužívajte priečinok na disku sformátovanom vo formáte VFAT/FAT32. "
#~ "Znaky /, | a \\ nie sú prijateľné a budú nahradené podtrhovníkmi."

#~ msgid "Shutdown computer when disc is done"
#~ msgstr "Vypnúť počítač po dokončení vytvárania disku"

#~ msgid "Burn"
#~ msgstr "Vypáliť"

#~ msgid "Job done!"
#~ msgstr "Úloha dokončená!"

#~ msgid "Debugging data"
#~ msgstr "Ladiace údaje"

#~ msgid "There was an error while creating the disc."
#~ msgstr "Vyskytla sa chyba počas vytvárania disku."

#~ msgid "Show this title in the menu"
#~ msgstr "Zobraziť tento titul v ponuke"

#~ msgid "<b>Title</b>"
#~ msgstr "<b>Titul</b>"

#~ msgid "Path:"
#~ msgstr "Cesta:"

#~ msgid "Original size (pixels):"
#~ msgstr "Pôvodná veľkosť (v pixeloch):"

#~ msgid "Original length (seconds):"
#~ msgstr "Pôvodná dĺžka (v sekundách):"

#~ msgid "Original aspect ratio:"
#~ msgstr "Pôvodný pomer strán:"

#~ msgid "Frames per second:"
#~ msgstr "Snímkov za sekundu:"

#~ msgid "Original audio rate (Kbits/sec):"
#~ msgstr "Pôvodný dátový tok zvuku (Kb/s):"

#~ msgid "Original video rate (Kbits/sec):"
#~ msgstr "Pôvodný dátový tok videa (Kb/s):"

#~ msgid "<b>File info</b>"
#~ msgstr "<b>Informácie o súbore</b>"

#~ msgid "column"
#~ msgstr "stĺpec"

#~ msgid "Select all"
#~ msgstr "Vybrať všetky"

#~ msgid "Unselect all"
#~ msgstr "Zrušiť výber"

#~ msgid "<b>Set properties to these files</b>"
#~ msgstr "<b>Nastaviť vlastnosti týmto súborom</b>"

#~ msgid "PAL/SECAM"
#~ msgstr "PAL/SECAM"

#~ msgid "NTSC"
#~ msgstr "NTSC"

#~ msgid "<b>Video format</b>"
#~ msgstr "<b>Formát videa</b>"

#~ msgid "300"
#~ msgstr "300"

#~ msgid "Reset"
#~ msgstr "Vynulovať"

#~ msgid "<b>Volume (%)</b>"
#~ msgstr "<b>Hlasitosť (%)</b>"

#~ msgid "Automatic"
#~ msgstr "Automaticky"

#~ msgid "5000"
#~ msgstr "5000"

#~ msgid "<b>Video rate (KBits/sec)</b>"
#~ msgstr "<b>Dátový tok videa (Kb/s)</b>"

#~ msgid "224"
#~ msgstr "224"

#~ msgid "<b>Audio rate (KBits/sec)</b>"
#~ msgstr "<b>Dátový tok zvuku (Kb/s)</b>"

#~ msgid "5"
#~ msgstr "5"

#~ msgid "Size (in minutes) for each chapter:"
#~ msgstr "Dĺžka (v minútach) pre každú kapitolu:"

#~ msgid "Split the file in chapters (for easy seeking)"
#~ msgstr "Rozdeliť súbor na kapitoly (pre uľahčenie posunu)"

#~ msgid "Manual chapter list (split by comma in mm:ss format):"
#~ msgstr "Ručný zoznam kapitol (rozdelené čiarkami vo formáte mm:ss):"

#~ msgid "<b>Division in chapters</b>"
#~ msgstr "<b>Rozdelenie na kapitoly</b>"

#~ msgid "General"
#~ msgstr "Všeobecné"

#~ msgid "Language"
#~ msgstr "Jazyk"

#~ msgid "Subtitle"
#~ msgstr "Titulky"

#~ msgid "Font size:"
#~ msgstr "Veľkosť písma:"

#~ msgid "Force subtitles"
#~ msgstr "Vynútiť titulky"

#~ msgid "Fill color:"
#~ msgstr "Farba výplne:"

#~ msgid "Outline color:"
#~ msgstr "Farba obrysu:"

#~ msgid "Outline thickness:"
#~ msgstr "Hrúbka obrysu:"

#~ msgid "Subtitles"
#~ msgstr "Titulky"

#~ msgid "1920x1080"
#~ msgstr "1920x1080"

#~ msgid "1280x720"
#~ msgstr "1280x720"

#~ msgid "720x480"
#~ msgstr "720x480"

#~ msgid "704x480"
#~ msgstr "704x480"

#~ msgid "480x480"
#~ msgstr "480x480"

#~ msgid "352x480"
#~ msgstr "352x480"

#~ msgid "352x240"
#~ msgstr "352x240"

#~ msgid "720x576"
#~ msgstr "720x576"

#~ msgid "704x576"
#~ msgstr "704x576"

#~ msgid "480x576"
#~ msgstr "480x576"

#~ msgid "352x576"
#~ msgstr "352x576"

#~ msgid "352x288"
#~ msgstr "352x288"

#~ msgid "<b>Final size</b>"
#~ msgstr "<b>Výsledná veľkosť</b>"

#~ msgid "4:3"
#~ msgstr "4:3"

#~ msgid "16:9"
#~ msgstr "16:9"

#~ msgid "<b>Aspect ratio</b>"
#~ msgstr "<b>Pomer strán</b>"

#~ msgid "Horizontal mirror"
#~ msgstr "Zrkadliť vodorovne"

#~ msgid "Vertical mirror"
#~ msgstr "Zrkadliť zvislo"

#~ msgid "<b>Mirror</b>"
#~ msgstr "<b>Zrkadlenie</b>"

#~ msgid "No rotation"
#~ msgstr "Bez otočenia"

#~ msgid "90 degrees (clockwise)"
#~ msgstr "O 90 stupňov (doprava)"

#~ msgid "180 degrees"
#~ msgstr "O 180 stupňov"

#~ msgid "90 degrees (counter-clockwise)"
#~ msgstr "O 90 stupňov (doľava)"

#~ msgid "<b>Rotation</b>"
#~ msgstr "<b>Otočenie</b>"

#~ msgid "Add black bars"
#~ msgstr "Pridať čierne pruhy"

#~ msgid "Scale picture"
#~ msgstr "Zmeniť mierku obrazu"

#~ msgid "Cut picture"
#~ msgstr "Orezať obraz"

#~ msgid "<b>Scaling mode</b>"
#~ msgstr "<b>Režim zmeny mierky</b>"

#~ msgid "Video options"
#~ msgstr "Voľby obrazu"

#~ msgid "Use dual pass encoding (better quality, much slower conversion)"
#~ msgstr ""
#~ "Použiť dvojprechodové kódovanie (lepšia kvalita, oveľa pomalšia konverzia)"

#~ msgid "<b>Two pass encoding</b>"
#~ msgstr "<b>Dvojprechodové  kódovanie</b>"

#~ msgid "Don't deinterlace"
#~ msgstr "Neodstraňovať prekladanie"

#~ msgid "FFMpeg deinterlacing filter"
#~ msgstr "Filter FFMpeg pre odstránenie prekladania"

#~ msgid "YADIF deinterlacing filter"
#~ msgstr "Filter YADIF pre odstránenie prekladania"

#~ msgid "<b>Deinterlacing</b>"
#~ msgstr "<b>Odstránenie prekladania</b>"

#~ msgid "Quality"
#~ msgstr "Kvalita"

#~ msgid "Audio delay (in seconds):"
#~ msgstr "Oneskorenie zvuku (v sekundách):"

#~ msgid "Create DVD with 5.1 channel sound"
#~ msgstr "Vytvoriť DVD s 5.1 kanálovým zvukom"

#~ msgid ""
#~ "This file already has AC3 sound (copy audio data instead of recompress it)"
#~ msgstr ""
#~ "Tento súbor už má zvuk vo formáte AC3 (skopíruje zvukové údaje namiesto "
#~ "ich opätovnej kompresie)"

#~ msgid "<b>Audio</b>"
#~ msgstr "<b>Zvuk</b>"

#~ msgid "Audio"
#~ msgstr "Zvuk"

#~ msgid "Stop reproduction/show disc menu"
#~ msgstr "Zastaviť prehrávanie/zobraziť ponuku disku"

#~ msgid "Play the first title"
#~ msgstr "Prehrať prvý titul"

#~ msgid "Play the previous title"
#~ msgstr "Prehrať predchádzajúci titul"

#~ msgid "Play this title again (loop)"
#~ msgstr "Prehrať tento titul ešte raz (slučka)"

#~ msgid "Play the next title"
#~ msgstr "Prehrať nasledujúci titul"

#~ msgid "Play the last title"
#~ msgstr "Prehrať posledný titul"

#~ msgid "<b>Action to perform when this title ends</b>"
#~ msgstr "<b>Akcia, ktorá sa má vykonať, keď titul skončí</b>"

#~ msgid "Actions"
#~ msgstr "Akcie"

#~ msgid "This file is already a DVD/xCD-suitable MPEG-PS file"
#~ msgstr "Súbor je už vo formáte MPEG-PS vhodnom pre DVD/xCD"

#~ msgid "Repack audio and video without reencoding (useful for VOB files)"
#~ msgstr ""
#~ "Znovu zabalí zvuk a video bez prekódovania (užitočné pri súboroch VOB)"

#~ msgid "Use a GOP of 12 frames (improves compatibility)"
#~ msgstr "Použiť GOP o 12 snímkoch (zvyšuje kompatibilitu)"

#~ msgid "<b>Special</b>"
#~ msgstr "<b>Špeciálne</b>"

#~ msgid "Misc"
#~ msgstr "Rôzne"

#~ msgid "<b>Options</b>"
#~ msgstr "<b>Voľby</b>"

#~ msgid "Preview"
#~ msgstr "Náhľad"

#~ msgid "Devede"
#~ msgstr "Devede"

#~ msgid "_File"
#~ msgstr "_Súbor"

#~ msgid "_Edit"
#~ msgstr "_Upraviť"

#~ msgid "Set multiproperties"
#~ msgstr "Nastaviť hromadne vlastnosti"

#~ msgid "_Help"
#~ msgstr "_Pomoc"

#~ msgid "Show documentation"
#~ msgstr "Zobraziť dokumentáciu"

#~ msgid "Title"
#~ msgstr "Titul"

#~ msgid "Duration"
#~ msgstr "Trvanie"

#~ msgid "<b>Files</b>"
#~ msgstr "<b>Súbory</b>"

#~ msgid "Media size:"
#~ msgstr "Veľkosť média:"

#~ msgid "Adjust disc usage"
#~ msgstr "Prispôsobiť využitie disku"

#~ msgid "<b>Disc usage</b>"
#~ msgstr "<b>Využitie disku</b>"

#~ msgid "PAL"
#~ msgstr "PAL"

#~ msgid "<b>Default format</b>"
#~ msgstr "<b>Predvolený formát</b>"

#~ msgid "Create a menu with the titles"
#~ msgstr "Vytvoriť ponuku s titulmi"

#~ msgid "Menu options"
#~ msgstr "Voľby ponuky"

#~ msgid "<b>Menus</b>"
#~ msgstr "<b>Ponuky</b>"

#~ msgid "Text:"
#~ msgstr "Text:"

#~ msgid "Font:"
#~ msgstr "Písmo:"

#~ msgid "Text color:"
#~ msgstr "Farba textu:"

#~ msgid "Shadow color:"
#~ msgstr "Farba tieňa:"

#~ msgid "Vertical position (percent):"
#~ msgstr "Zvislá pozícia (v percentách):"

#~ msgid "Horizontal position (percent):"
#~ msgstr "Vodorovná pozícia (v percentách):"

#~ msgid "<b>Menu title</b>"
#~ msgstr "<b>Názov ponuky</b>"

#~ msgid "Default background"
#~ msgstr "Predvolené pozadie"

#~ msgid "<b>Menu background</b>"
#~ msgstr "<b>Pozadie ponuky</b>"

#~ msgid "Menu without sound"
#~ msgstr "Ponuka bez zvuku"

#~ msgid "Audio format:"
#~ msgstr "Formát zvuku:"

#~ msgid "MP2"
#~ msgstr "MP2"

#~ msgid "AC3"
#~ msgstr "AC3"

#~ msgid "<b>Menu music</b>"
#~ msgstr "<b>Hudba ponuky</b>"

#~ msgid "<b>Horizontal</b>"
#~ msgstr "<b>Vodorovne</b>"

#~ msgid "<b>Margins (percent)</b>"
#~ msgstr "<b>Okraje (v percentách)</b>"

#~ msgid "Left"
#~ msgstr "Vľavo"

#~ msgid "Center"
#~ msgstr "V strede"

#~ msgid "Right"
#~ msgstr "Vpravo"

#~ msgid "<b>Menu position</b>"
#~ msgstr "<b>Pozícia ponuky</b>"

#~ msgid "Unselected titles:"
#~ msgstr "Nevybraté tituly:"

#~ msgid "Shadows:"
#~ msgstr "Tiene:"

#~ msgid "Selected title:"
#~ msgstr "Vybratý titul:"

#~ msgid "Background color:"
#~ msgstr "Farba pozadia:"

#~ msgid "<b>Menu font and colors</b>"
#~ msgstr "<b>Farby a písmo ponuky</b>"

#~ msgid "Show menu at disk startup"
#~ msgstr "Zobraziť ponuku pri spustení disku"

#~ msgid "Jump to the first title at startup"
#~ msgstr "Pri spustení skočiť na prvý titul"

#~ msgid "Provide \"Play All\" option"
#~ msgstr "Poskytnúť voľbu „Prehrať všetko“"

#~ msgid "<b>Disc startup options</b>"
#~ msgstr "<b>Voľby pre spustenie disku</b>"

#~ msgid "Show titles as selected"
#~ msgstr "Zobraziť tituly ako vybraté"

#~ msgid "<b>Preview</b>"
#~ msgstr "<b>Náhľad</b>"

#~ msgid "<b>Menu preview</b>"
#~ msgstr "<b>Náhľad ponuky</b>"

#~ msgid "Programs needed by Devede NG"
#~ msgstr "Programy vyžadované aplikáciou Devede NG"

#~ msgid ""
#~ "<b>Preview video</b>\n"
#~ "\n"
#~ "Devede will create a preview with the selected parameters, so you will be "
#~ "able to check the video quality, audio sync and so on."
#~ msgstr ""
#~ "<b>Náhľad videa</b>\n"
#~ "\n"
#~ "Program Devede vytvorí náhľad s vybratými parametrami, takže budete môcť "
#~ "skontrolovať kvalitu videa, synchronizáciu zvuku a podobne."

#~ msgid "Preview length:"
#~ msgstr "Dĺžka náhľadu:"

#~ msgid "Creating..."
#~ msgstr "Vytváranie..."

#~ msgid "Creating disc"
#~ msgstr "Vytvára sa disk"

#~ msgid "Project progress"
#~ msgstr "Priebeh vytvárania projektu"

#~ msgid "Devede NG"
#~ msgstr "Devede NG"

#~ msgid "<b>Choose the disc type you want to create with Devede</b>"
#~ msgstr "<b>Vyberte druh disku, ktorý chcete vytvoriť pomocou DeVeDe</b>"

#~ msgid ""
#~ "<b>Video DVD</b>\n"
#~ "Creates a video DVD suitable for all DVD home players"
#~ msgstr ""
#~ "<b>Video DVD</b>\n"
#~ "Vytvorí video DVD vhodné pre všetky domáce prehrávače diskov DVD"

#~ msgid ""
#~ "<b>VideoCD</b>\n"
#~ "Creates a VideoCD, with a picture quality equivalent to VHS"
#~ msgstr ""
#~ "<b>VideoCD</b>\n"
#~ "Vytvorí VideoCD s kvalitou obrazu porovnateľnou s VHS"

#~ msgid ""
#~ "<b>Super VideoCD</b>\n"
#~ "Creates a VideoCD with better picture quality"
#~ msgstr ""
#~ "<b>Super VideoCD</b>\n"
#~ "Vytvorí VideoCD s lepšou kvalitou obrazu"

#~ msgid ""
#~ "<b>China VideoDisc</b>\n"
#~ "Another kind of Super VideoCD"
#~ msgstr ""
#~ "<b>Čínsky VideoDisc</b>\n"
#~ "Ďalší druh Super VideoCD"

#~ msgid ""
#~ "<b>DivX / MPEG-4</b>\n"
#~ "Creates files compliant with DivX home players"
#~ msgstr ""
#~ "<b>DivX/MPEG-4</b>\n"
#~ "Vytvorí súbory kompatibilné s domácimi prehrávačmi videí vo formáte DivX"

#~ msgid ""
#~ "<b>Matroska / H.264</b>\n"
#~ "Creates H.264 files in a MKV container"
#~ msgstr ""
#~ "<b>Matroska / H.264</b>\n"
#~ "Vytvorí súbory vo formáte H.264 v kontajneri MKV"

#~ msgid "Programs needed by Devede"
#~ msgstr "Programy vyžadované aplikáciou Devede"

#~ msgid "<b>Multicore CPUs</b>"
#~ msgstr "<b>Viacjadrové procesory</b>"

#~ msgid "Temporary files folder:"
#~ msgstr "Priečinok pre dočasné súbory:"

#~ msgid "<b>Folders</b>"
#~ msgstr "<b>Priečinky</b>"

#~ msgid "Play preview:"
#~ msgstr "Prehranie náhľadu:"

#~ msgid "Get video info:"
#~ msgstr "Získanie informácií o videu:"

#~ msgid "Convert menus:"
#~ msgstr "Konvertovanie ponúk:"

#~ msgid "Convert videos:"
#~ msgstr "Konvertovanie videí:"

#~ msgid "Burn ISOs:"
#~ msgstr "Vypálenie obrazov ISO:"

#~ msgid "Disc types supported:"
#~ msgstr "Podporované typy diskov:"

#~ msgid "Create ISOs:"
#~ msgstr "Vytvorenie obrazov ISO:"

#~ msgid "<b>Backends</b>"
#~ msgstr "<b>Obslužné programy</b>"

#~ msgid "Title properties"
#~ msgstr "Vlastnosti titulu"

#~ msgid "<b>Title's name</b>"
#~ msgstr "<b>Názov titulu</b>"
